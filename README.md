# Integrating Azure AKS with Azure Key Vault

### Requirements
- Azure CLI > 2.27
- User with necessary permissions (AKS/KV)

### Step 1 - Install AKS-preview extension
```
az extension add --name aks-preview
```

### Step 2 - Register the new feature
```
az feature register --namespace "Microsoft.ContainerService" --name AKS-AzureKeyvaultSecretsProvider
```

### Step 3 - Check if the resource was registered successfully
```
az feature list -o table --query "[?contains(name, 'Microsoft.ContainerService/AKS-AzureKeyvaultSecretsProvider')].{Name:name,State:properties.state}"
```

### Step 4 - Enable the new feature
```
az provider register --namespace Microsoft.ContainerService
```

### Step 5 - Install the add-on on cluster (Secret Store CSI Driver)
```
az aks enable-addons --resource-group RG_NAME --name Cluster_Name --addons azure-keyvault-secrets-provider
```

### Step 6 - Check the pods created in kube-system namespace
```
kubectl get pods -n kube-system

NAME                                        READY   STATUS
aks-secrets-store-csi-driver-w7c79          3/3     Running
aks-secrets-store-provider-azure-jn7kn      1/1     Running
```

### Step 7 - Get ID of Identity configured on AKS
```
mid=$(az aks show --resource-group RG_NAME --name Cluster_Name --query identityProfile.kubeletidentity.clientId -o tsv)
```

### Step 8 - Apply permission to access Key Vault on AKS Identity
```
az keyvault set-policy -n keyvaultname --key-permissions get --spn $mid
az keyvault set-policy -n keyvaultname --secret-permissions get --spn $mid
az keyvault set-policy -n keyvaultname --certificate-permissions get --spn $mid
```

### Step 9 - Create secrets on Azure Key Vault
```
az keyvault secret set --name Secret_Name --vault-name Key_Vault_Name --value Value_Of_Secret
``` 

### Step 10 - Apply yaml file to deploy SecretProviderClass where will let the cluster know about the key vault and what identity to use to get the information from KV
#### Obs:
```Fill the fields that have #(Comment) at the end of line```

```Value of objectName must match with the name of secret created```
```yaml
apiVersion: secrets-store.csi.x-k8s.io/v1alpha1
kind: SecretProviderClass
metadata:
  name: azure-kvname-user-msi
spec:
  provider: azure
  parameters:
    usePodIdentity: "false"
    useVMManagedIdentity: "true"
    userAssignedIdentityID: "<client id of user assigned identity>" # ID of AKS Identity
    keyvaultName: "kvname" # Name of Key Vault
    cloudName: ""
    objects:  |
      array:
        - |
          objectName: secret1 # Value of secret created in Key Vault
          objectType: secret
          objectVersion: ""
        - |
          objectName: secret2 # Value of secret created in Key Vault
          objectType: secret
          objectVersion: ""
    tenantId: "tid" # Tenant ID of Azure Account
```

### Step 11 - Mount volume using the driver CSI
```yaml
kind: Pod
apiVersion: v1
metadata:
  name: nginx
  namespace: default
spec:
  containers:
    - name: nginx
      image: nginx
      command:
        - "/bin/sleep"
        - "10000"
      volumeMounts:
      - name: secrets-store01-inline
        mountPath: "/mnt/secrets-store"
        readOnly: true
  volumes:
    - name: secrets-store01-inline
      csi:
        driver: secrets-store.csi.k8s.io
        readOnly: true
        volumeAttributes:
          secretProviderClass: "azure-kvname-user-msi"
```

### Step 12 - Check the mount volume inside container
```
kubectl exec -it nginx -n default sh
$ ls /mnt/secrets-store
```

# Using mount secrets as environment variables

### Step 1 - Create SecretProviderClass specifying secretObjects
```yaml
apiVersion: secrets-store.csi.x-k8s.io/v1alpha1
kind: SecretProviderClass
metadata:
  name: azure-kvname-user-msi
  namespace: development
spec:
  provider: azure
  secretObjects:
  - secretName: webhook
    type: Opaque
    data:
    - objectName: WEBHOOK-SPRING-REDIS-PASSWORD
      key: SPRING-REDIS-PASSWORD
  parameters:
    usePodIdentity: "false"
    useVMManagedIdentity: "true"
    userAssignedIdentityID: "" # ID with permission to read values from Azure Key Vault
    keyvaultName: "kv-dev"
    cloudName: ""
    objects:  |
      array:
        - |
          objectName: WEBHOOK-SPRING-REDIS-PASSWORD
          objectType: secret
          objectVersion: ""
    tenantId: "" # Tenant of Azure Account
```

### Step 2 - Create POD/Deloyment to get parameters from secret
```yaml
kind: Pod
apiVersion: v1
metadata:
  name: nginx
  namespace: default
spec:
  containers:
    - name: nginx
      image: nginx
      env:
      - name: DB_PASSWORD # Value of variable using in your application
        valueFrom:
          secretKeyRef:
            name: webhook # Name of secret created in SecretProviderClass
            key: SPRING-REDIS-PASSWORD # Key associated with secrets
      volumeMounts:
      - name: secrets-store01-inline
        mountPath: "/mnt/secrets-store" # Mount directory inside container
        readOnly: true
  volumes:
    - name: secrets-store01-inline
      csi:
        driver: secrets-store.csi.k8s.io
        readOnly: true
        volumeAttributes:
          secretProviderClass: "azure-kvname-user-msi" # Name of previously created volume
```

# Resource communication topology

![AzureKV](img/aks-kv.jpg) 